package org.pan.ui;

import io.advantageous.guicefx.LoadedBy;
import io.advantageous.guicefx.Presents;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;

import java.util.Optional;

@LoadedBy("Dashboard.fxml")
public class Dashboard {
    public TabPane tabPane;
    @FXML
    public VBox root;

    public Parent getRoot() {
        return root;
    }

    public void exitSystem(ActionEvent actionEvent) {

    }
    public void btn_deviceManage(ActionEvent actionEvent) {
        addTab("DeviceManage.fxml", "系统设备管理");
    }

    public void btn_userGroupManage(ActionEvent actionEvent) {
        addTab("UserGroupManage.fxml", "组织架构管理");
    }

    public void btn_userManage(ActionEvent actionEvent) {
        addTab("UserManage.fxml", "卡片用户管理");
    }

    public void btn_cardDevicePrivilege(ActionEvent actionEvent) {

    }

    public void btn_cardRecord(ActionEvent actionEvent) {

    }


    private void addTab(String fxml, String title) {
        Optional<Tab> optionalTab = tabPane.getTabs().stream().filter(f -> f.getText().equals(title)).findAny();
        if (optionalTab.isPresent()) {
            tabPane.getSelectionModel().select(optionalTab.get());
            return;
        }

        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxml));
            Tab tab = new Tab(title, root);
            tabPane.getTabs().add(tab);

            tabPane.getSelectionModel().select(tab);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
