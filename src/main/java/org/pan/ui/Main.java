package org.pan.ui;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.sun.javafx.tk.ImageLoader;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.pan.MyApplicationModule;

public class Main extends Application {

    @Inject
    private Dashboard dashboard;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Guice.createInjector(new MyApplicationModule()).injectMembers(this);
        primaryStage.setScene(new Scene(dashboard.getRoot()));
        primaryStage.setTitle("一卡通管理");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Register-icon.png")));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
