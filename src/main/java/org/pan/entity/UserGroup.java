package org.pan.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class UserGroup extends EntityObject{
    @Column
    private String groupName;
    @ManyToOne
    private UserGroup groupParent;

}
